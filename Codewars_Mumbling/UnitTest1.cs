﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace Codewars_Mumbling
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void s_is_b_should_return_B()
        {
            var s = "b";
            AccumShouldBe(s, "B");
        }

        private static void AccumShouldBe(string s, string expected)
        {
            Assert.AreEqual(expected, Accumul.Accum(s));
        }

        [TestMethod]
        public void s_is_bc_should_return_B_Cc()
        {
            var s = "bc";
            AccumShouldBe(s, "B-Cc");
        }

        [TestMethod]
        public void s_is_bC_should_return_B_Cc()
        {
            var s = "bC";
            AccumShouldBe(s, "B-Cc");
        }

        [TestMethod]
        public void s_is_bce_should_return_B_Cc_Eee()
        {
            var s = "bce";
            AccumShouldBe(s, "B-Cc-Eee");
        }
    }

    public class Accumul
    {
        public static String Accum(string s)
        {
            var result = s.Select((c, i) => ToUpperChar(c) + GetRepeatLetters(c, i));
            return string.Join("-", result);
        }

        private static string GetRepeatLetters(char letter, int repeatTimes)
        {
            return string.Concat(Enumerable.Repeat(letter.ToString().ToLower(), repeatTimes));
        }

        private static string ToUpperChar(char letter)
        {
            return letter.ToString().ToUpper();
        }
    }
}